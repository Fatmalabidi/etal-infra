 #!/bin/bash

sudo rpm -i https://download.elastic.co/elasticsearch/release/org/elasticsearch/distribution/rpm/elasticsearch/2.3.3/elasticsearch-2.3.3.rpm

sudo chkconfig --add elasticsearch

cd /usr/share/elasticsearch/ 
sudo bin/plugin install cloud-aws
ES_HEAP_SIZE=15g 

MAX_LOCKED_MEMORY=unlimited
echo "cluster.name:esonaws 
bootstrap.mlockall: true 
discovery.zen.ping.unicast.hosts: [localhost,…] 
network.host: localhost" > /etc/elasticsearch/elasticsearch.yml

sudo service elasticsearch start

yum update -y
yum install -y httpd.x86_64
systemctl start httpd.service
systemctl enable httpd.service
echo "Hello World from $(hostname -f)" > /var/www/html/index.html

yum install ec2-instance-connect
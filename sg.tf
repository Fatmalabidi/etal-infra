# resource "aws_security_group_rule" "outbound-allow-all-es" {
#   type              = "egress"
#   to_port           = 0
#   protocol          = "-1"
#   from_port         = 0
#   security_group_id = aws_security_group.etal-elasticsearch-sg.id
# }


/***************************************************************************
    Elasticsearch security group and rules
***************************************************************************/

resource "aws_security_group" "etal-elasticsearch-sg" {
  name   = "ETAL-elasticsearch-sg"
  vpc_id = aws_vpc.etal-vpc.id
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "ETAL elasticsearch-sg-${var.env}"
  }
}

resource "aws_security_group_rule" "allow-ecs-access-to-elasticsearch" {
  type                     = "ingress"
  description              = "allow ECS Task access to elasticserach on EC2"
  from_port                = var.es_port
  to_port                  = var.es_port
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.etal-ecs-sg.id
  security_group_id        = aws_security_group.etal-elasticsearch-sg.id
}

resource "aws_security_group_rule" "allow-http-access-to-elasticsearch" {
  type              = "ingress"
  description       = "allow http access to elasticserach on EC2"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.etal-elasticsearch-sg.id
}


resource "aws_security_group_rule" "allow-local-ssh-to-elasticsearch" {
  type              = "ingress"
  description       = "allow local ssh access to elasticserach on EC2"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["197.0.248.127/32"]
  security_group_id = aws_security_group.etal-elasticsearch-sg.id
}

resource "aws_security_group_rule" "allow-ssh-to-elasticsearch" {
  type              = "ingress"
  description       = "allow ssh access to elasticserach on EC2"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = [aws_vpc.etal-vpc.cidr_block]
  security_group_id = aws_security_group.etal-elasticsearch-sg.id
}



/***************************************************************************
    Redis security group and rules
***************************************************************************/
resource "aws_security_group" "etal-redis-sg" {
  name   = "ETAL-redis-sg"
  vpc_id = aws_vpc.etal-vpc.id
  tags = {
    Name = "ETAL redis-sg-${var.env}"
  }
}

resource "aws_security_group_rule" "allow-es-access-to-redis" {
  description              = "allow the access to redis from elastic serach sg"
  type                     = "ingress"
  from_port                = var.redis_port
  to_port                  = var.redis_port
  protocol                 = "tcp"
  security_group_id        = aws_security_group.etal-redis-sg.id
  source_security_group_id = aws_security_group.etal-elasticsearch-sg.id
}

resource "aws_security_group_rule" "allow-access-to-redis" {
  description              = "allow the access to redis from ECS task sg"
  type                     = "ingress"
  from_port                = var.redis_port
  to_port                  = var.redis_port
  protocol                 = "tcp"
  security_group_id        = aws_security_group.etal-redis-sg.id
  source_security_group_id = aws_security_group.etal-ecs-sg.id
}


/***************************************************************************
    Postgresql security group and rules
***************************************************************************/


resource "aws_security_group" "etal-postgres-sg" {
  name   = "ETAL-postgres-sg"
  vpc_id = aws_vpc.etal-vpc.id
  tags = {
    Name = "ETAL postgres-sg-${var.env}"
  }
}


resource "aws_security_group_rule" "allow-es-access-to-postgres" {
  description              = "allow the access to postgres from elasticsearch sg"
  type                     = "ingress"
  from_port                = var.db_port
  to_port                  = var.db_port
  protocol                 = "tcp"
  security_group_id        = aws_security_group.etal-postgres-sg.id
  source_security_group_id = aws_security_group.etal-elasticsearch-sg.id
}

resource "aws_security_group_rule" "allow-ecs-access-to-postgres" {
  description              = "allow the access to postgres from ECS Task sg"
  type                     = "ingress"
  from_port                = var.db_port
  to_port                  = var.db_port
  protocol                 = "tcp"
  security_group_id        = aws_security_group.etal-postgres-sg.id
  source_security_group_id = aws_security_group.etal-ecs-sg.id
}

resource "aws_security_group_rule" "allow-ip-access-to-postgres" {
  description       = "allow the access to postgres from my home IP"
  type              = "ingress"
  from_port         = var.db_port
  to_port           = var.db_port
  protocol          = "tcp"
  cidr_blocks       = ["${var.my_ip}/32"]
  security_group_id = aws_security_group.etal-postgres-sg.id
}


/***************************************************************************
    ECS task security group and rules
***************************************************************************/

resource "aws_security_group" "etal-ecs-sg" {
  name   = "ETAL-ecs-sg"
  vpc_id = aws_vpc.etal-vpc.id
  tags = {
    Name = "ETAL ecs-sg-${var.env}"
  }
}
resource "aws_security_group_rule" "allow-access-to-ecs" {
  description       = "allow the access to ecs from anywhere on port ${var.server_port}"
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = var.server_port
  to_port           = var.server_port
  protocol          = "tcp"
  security_group_id = aws_security_group.etal-ecs-sg.id
}

// TODO check https://registry.terraform.io/modules/alibaba/security-group/alicloud/latest

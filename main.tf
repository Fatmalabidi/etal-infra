provider "aws" {
  access_key = var.access
  secret_key = var.secret
  region     = var.region
}

resource "aws_kms_key" "etal-kms-key" {
  description             = "This key is used to encrypt bucket objects"
  deletion_window_in_days = 10
}

resource "aws_s3_bucket" "etal-s3" {
  bucket = "${var.bucket_name}-${var.env}"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.etal-kms-key.arn
        sse_algorithm     = "aws:kms"
      }
    }
  }
  tags = {
    env = var.env
  }
  versioning {
    enabled = true
  }
}


resource "tls_private_key" "etal-prv-key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

/***************************************************************************
    Elasticsearch configuration
***************************************************************************/


resource "aws_key_pair" "etal-key-pair" {
  key_name   = "etal-es-keypair"
  public_key = tls_private_key.etal-prv-key.public_key_openssh
  provider   = aws
  provisioner "local-exec" {
    command = "echo '${tls_private_key.etal-prv-key.private_key_pem}' >> ./etal-key-pair.pem"
  }
}

resource "aws_instance" "etal-ec2-es" {
  ami                                  = "ami-063d4ab14480ac177"
  instance_type                        = "t2.medium"
  availability_zone                    = "${var.region}a"
  subnet_id                            = aws_subnet.etal-public-subnet-a.id
  user_data                            = file("install_es.sh")
  associate_public_ip_address          = true
  vpc_security_group_ids               = [aws_security_group.etal-elasticsearch-sg.id]
  instance_initiated_shutdown_behavior = "terminate"
  key_name                             = aws_key_pair.etal-key-pair.key_name
  depends_on = [
    aws_security_group.etal-elasticsearch-sg
  ]
  tags = {
    Name = "ETAL elasticsearch ${var.env}"
  }
}


/***************************************************************************
    Redis configuration
***************************************************************************/

resource "aws_elasticache_cluster" "etal-redis" {
  cluster_id           = "etal-${var.env}"
  engine               = "redis"
  node_type            = "cache.r6g.large"
  num_cache_nodes      = 1
  parameter_group_name = "default.redis6.x"
  engine_version       = "6.x"
  port                 = 6379
  depends_on = [
    aws_subnet.etal-public-subnet-a
  ]
  subnet_group_name  = aws_elasticache_subnet_group.etal-es-subnet-grp.name
  security_group_ids = [aws_security_group.etal-redis-sg.id]
  tags = {
    Name = "ETAL redis ${var.env}"
  }
}

/***************************************************************************
    PostgreSQL configuration
***************************************************************************/


resource "aws_db_instance" "etal-db" {
  identifier        = "etal-db-${var.env}"
  allocated_storage = 10
  engine            = "postgres"
  engine_version    = "12.5"
  instance_class    = "db.t2.micro"
  depends_on = [
    aws_subnet.etal-public-subnet-a
  ]
  db_subnet_group_name = aws_db_subnet_group.etal-db-subnet-grp.name
  #The name of the database to create when the DB instance is created.
  name = "etal_core_${var.env}"
  #TODO remove hardcoded cred
  username               = "postgresuser"
  password               = "postgrespass"
  port                   = 5432
  skip_final_snapshot    = true
  publicly_accessible    = true
  storage_type           = "gp2"
  vpc_security_group_ids = [aws_security_group.etal-postgres-sg.id]

  tags = {
    Name = "ETAL postgres ${var.env}"
  }
}

/***************************************************************************
    ECS configuration
***************************************************************************/

resource "aws_ecs_cluster" "etal-cluster" {
  name = "etal-cluster-${var.env}"
}

resource "aws_ecs_task_definition" "etal-td" {
  family       = "etal-td-${var.env}"
  cpu          = 1024
  memory       = 2048
  network_mode = "awsvpc"

  #TODO create custom roles
  task_role_arn      = "arn:aws:iam::657215334401:role/ecsTaskExecutionRole"
  execution_role_arn = "arn:aws:iam::657215334401:role/ecsTaskExecutionRole"
  requires_compatibilities = [
    "FARGATE"
  ]
  container_definitions = jsonencode([
    {
      name      = "etal"
      image     = "657215334401.dkr.ecr.eu-west-1.amazonaws.com/stelace-demo:dockerprod"
      essential = true
      "environment" : [
        {
          "name" : "INSTALLED_PLUGINS",
          "value" : "https://github.com/stelace/stelace-search-filter-dsl-parser.git#0.3.0"
        },
        {
          "name" : "POSTGRES_ADMIN_USER",
          "value" : "${aws_db_instance.etal-db.username}"
        },
        {
          "name" : "POSTGRES_DB",

          "value" : "${aws_db_instance.etal-db.name}"
        },
        {
          "name" : "POSTGRES_HOST",
          "value" : "${aws_db_instance.etal-db.address}"
        },
        {
          "name" : "POSTGRES_PASSWORD",
          "value" : "${aws_db_instance.etal-db.password}"
        },
        {
          "name" : "POSTGRES_USER",
          "value" : "${aws_db_instance.etal-db.username}"
        },
        {
          "name" : "REDIS_DBNUM",
          "value" : "0"
        },
        //TODO cmplete 
        {
          "name" : "REDIS_HOST",
          "value" : "${aws_elasticache_cluster.etal-redis.cache_nodes.0.address}"
        },
        {
          "name" : "REDIS_PASSWORD",
          "value" : "todo"
        },
        {
          "name" : "REDIS_TLS",
          "value" : "false"
        },
        {
          "name" : "REMOTE_STORE",
          "value" : "true"
        },
        {
          "name" : "SERVER_PORT",
          "value" : "4100"
        },
        {
          "name" : "STELACE_API_URL",
          "value" : "127.0.0.1:4100"
        },
        {
          "name" : "POSTGRES_PORT",
          "value" : "5432"
        },
        {
          "name" : "REDIS_PORT",
          "value" : "6379"
        },
      ]
      portMappings = [
        {
          containerPort = 4100
        }
      ]
    },
    {
      name      = "httpd"
      image     = "httpd:latest"
      essential = false

      portMappings = [
        {
          containerPort = 80
        }
      ]
    }
  ])
}

resource "aws_ecs_service" "etal-svc" {
  name                               = "etal-service-${var.env}"
  desired_count                      = 1
  deployment_minimum_healthy_percent = 50
  deployment_maximum_percent         = 200
  launch_type                        = "FARGATE"
  scheduling_strategy                = "REPLICA"
  cluster                            = aws_ecs_cluster.etal-cluster.id
  task_definition                    = aws_ecs_task_definition.etal-td.arn
  network_configuration {
    subnets          = ["subnet-a242e5f8", "subnet-8eee9bc6"]
    assign_public_ip = true
  }
  tags = {
    Name = "ETAL ecs service ${var.env}"
  }
}


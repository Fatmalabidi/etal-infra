bucket_name = "tf-state-file"
region      = "eu-west-1"
my_ip       = "197.0.222.25"

redis_cluster_id = "etal-redis-cluster"
redis_port       = 6379

db_pass = "postgrespass"
db_user = "postgresuser"
db_port = 6543

server_port = 4100
es_port     = 9200

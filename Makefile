version= 0.0.1

SHELL=/bin/bash


plan:
	@cd dev && sudo terraform plan -out=output.txt -var="env=dev" -var="access=$(AWS_ACCESS_KEY)" -var="secret=$(AWS_SECRET_KEY)"

############# Prepparing directories commands ################

copy-test:
	@mkdir -p test && \cp -r *.* ./test
copy-dev:
	@mkdir -p dev && \cp -r *.* ./dev
copy-prod:
	mkdir -p prod && \cp -r -p *.* ./prod;

init-test: copy-test
	cd test &&  sudo terraform init;
init-dev: copy-dev
	cd dev &&  sudo terraform init;
init-prod:copy-test
	cd prod &&  sudo terraform init


copy-all: copy-test  copy-prod   copy-dev

init-all: copy-all  
	cd test && echo "init test"	&&  sudo terraform init ;\
	cd ../dev && echo "init dev"	&& 	sudo terraform init;\
	cd ../prod && echo "init prod"	&& 	sudo terraform init;

############# Apply commands ################

apply-test:init-test
	@cd test &&	sudo terraform apply -var="env=test" -var="access=$(AWS_ACCESS_KEY)" -var="secret=$(AWS_SECRET_KEY)" --auto-approve;

apply-dev: init-dev
	@cd dev && 	sudo terraform apply -var="env=dev" -var="access=$(AWS_ACCESS_KEY)" -var="secret=$(AWS_SECRET_KEY)" --auto-approve ;

apply-prod: init-prod
	@cd prod &&	sudo terraform apply -var="env=prod" -var="access=$(AWS_ACCESS_KEY)" -var="secret=$(AWS_SECRET_KEY)";
	push-s3-prod

apply-all: init-all apply-dev apply-test apply-prod 

############# Destroy commands ################

destroy-dev:
	@cd dev && sudo terraform destroy -var="env=dev" -var="access=$(AWS_ACCESS_KEY)" -var="secret=$(AWS_SECRET_KEY)" --auto-approve;
	sudo rm -rf dev 
destroy-test:
	@cd test && sudo terraform destroy -var="env=test"  -var="access=$(AWS_ACCESS_KEY)" -var="secret=$(AWS_SECRET_KEY)" --auto-approve;
	sudo rm -rf test
destroy-prod:
	@cd prod && sudo terraform destroy  -var="env=prod" -var="access=$(AWS_ACCESS_KEY)" -var="secret=$(AWS_SECRET_KEY)";
	sudo rm -rf prod
	
clean: destroy-dev destroy-test 
	sudo rm -rf dev test 

destroy-all: destroy-dev destroy-test destroy-prod clean


##########################

push-s3-prod:
	aws s3 cp .terraform.lock.hcl s3://state-file-prod

## Configure your AWS eccess keys and export them :
```shell   
  export AWS_ACCESS_KEY="<your_aws_access_key>"
```   
```shell   
 export AWS_SECRET_KEY="<your_aws_secret_key>"
``` 

To create `dev`,`test` and `prod` resources run:
  ```shell   
 make tf-apply-all
``` 
this command will create a folder for each envirement, initilise the terraform enviremenet for each folder then create resources based on the envirement variables.



to destroy the `test`  and `dev` envirements run:
  ```shell   
 make clean
``` 

to destroy the `test`, `dev`  and **`prod`** envirements run:
  ```shell   
 make tf-destroy-all
``` 

<!-- aws s3 cp - <target> [--options] -->
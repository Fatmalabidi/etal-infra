variable "secret" {
  description = "AWS secret key"
  sensitive   = true
}
variable "access" {
  description = "AWS access key"
  sensitive   = true
}
variable "region" {
  description = "AWS region"
}
variable "my_ip" {
  description = "local ip address"
}


variable "bucket_name" {
  description = "bucket where to save the state"
}
variable "env" {
  description = "env var entered from the command (default 'dev')"
  default     = "test"
}


variable "redis_cluster_id" {
  description = "the cache cluster id"
}
variable "redis_port" {
  description = "the cache port"
}

variable "db_pass" {
  description = "RDS postgres password"
  sensitive   = true
}
variable "db_user" {
  description = "RDS postgres user"
}
variable "db_port" {
  description = "RDS postgres port"
}

variable "es_port" {
  description = "Elasticsearch  port"
}
variable "server_port" {
  description = "serverI port"
}


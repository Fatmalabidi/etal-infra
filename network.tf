/*************************************************************
    configure VPC and a subnet for esach AZ
**************************************************************/
resource "aws_vpc" "etal-vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = "etal-vpc-${var.env}"
  }
}

resource "aws_subnet" "etal-public-subnet-a" {
  vpc_id            = aws_vpc.etal-vpc.id
  cidr_block        = "10.0.0.0/24"
  availability_zone = "${var.region}a"

  tags = {
    Name = "ETAL Public Subnet ${var.region}a"
  }
}

resource "aws_subnet" "etal-private-subnet-b" {
  vpc_id            = aws_vpc.etal-vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "${var.region}b"

  tags = {
    Name = "ETAL Private Subnet ${var.region}b"
  }
}

resource "aws_subnet" "etal-private-subnet-c" {
  vpc_id            = aws_vpc.etal-vpc.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "${var.region}c"

  tags = {
    Name = "ETAL Private Subnet ${var.region}c"
  }
}

/*************************************************************
    Configure internet GW
**************************************************************/

resource "aws_internet_gateway" "etal-vpc-gw" {
  vpc_id = aws_vpc.etal-vpc.id
  depends_on = [
    aws_vpc.etal-vpc
  ]
  tags = {
    Name = "ETAL VPC - Internet Gateway"
  }
}

/*************************************************************
    Configure the routing table 
**************************************************************/

resource "aws_route_table" "etal-vpc-routing-table" {
  vpc_id = aws_vpc.etal-vpc.id
  depends_on = [
    aws_vpc.etal-vpc
  ]
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.etal-vpc-gw.id
  }
  tags = {
    Name = "ETAL VPC Routing Table"
  }
}

resource "aws_route_table_association" "etal_vpc_us_east_1a_public" {
  subnet_id      = aws_subnet.etal-public-subnet-a.id
  route_table_id = aws_route_table.etal-vpc-routing-table.id
}
resource "aws_route_table_association" "etal_vpc_us_east_1b_public" {
  subnet_id      = aws_subnet.etal-private-subnet-b.id
  route_table_id = aws_route_table.etal-vpc-routing-table.id
}
resource "aws_route_table_association" "etal_vpc_us_east_1c_public" {
  subnet_id      = aws_subnet.etal-private-subnet-c.id
  route_table_id = aws_route_table.etal-vpc-routing-table.id
}


/*************************************************************
    Configure db and cache subnet groups
**************************************************************/

resource "aws_elasticache_subnet_group" "etal-es-subnet-grp" {
  name       = "etal-cache-subnet-${var.env}"
  subnet_ids = [aws_subnet.etal-public-subnet-a.id]
}

resource "aws_db_subnet_group" "etal-db-subnet-grp" {
  name       = "etal-db-subnet-${var.env}"
  subnet_ids = [aws_subnet.etal-public-subnet-a.id, aws_subnet.etal-private-subnet-b.id]
}
